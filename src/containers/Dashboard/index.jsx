import React, { Component } from 'react';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import Drawer from 'material-ui/Drawer';
import Dialog from 'material-ui/Dialog';
import MenuItem from 'material-ui/MenuItem';
import Snackbar from 'material-ui/Snackbar';
import FloatingActionButton from 'material-ui/FloatingActionButton';
import ContentAdd from 'material-ui/svg-icons/content/add';
import FlatButton from 'material-ui/FlatButton';
import AppBar from 'material-ui/AppBar';
import IconButton from 'material-ui/IconButton';
import NavigationClose from 'material-ui/svg-icons/navigation/close';
import DashboardIcon from 'material-ui/svg-icons/action/dashboard';
import ContactIcon from 'material-ui/svg-icons/action/perm-contact-calendar';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { FormEventsEmitter } from 'react-components-form';
import { FleetDataBase, FleetBoard } from "./../index";
import { FleetForm } from './../../components/index';
import * as actions from './../../actions/userActions';


class Dashboard extends Component {
  constructor(props) {
    super(props);
    this.state = {
      open: false,
      activeComponent: 'fleetBoard',
      modalOpen: false,
    };
    this.drawerToggle = this.drawerToggle.bind(this);
    this.switchMainContainer = this.switchMainContainer.bind(this);
    this.changeMainComponent = this.changeMainComponent.bind(this);
    this.handleOpen = this.handleOpen.bind(this);
    this.handleClose = this.handleClose.bind(this);
  }
  drawerToggle() {
    this.setState({
      open: !this.state.open,
    });
  }
  handleOpen() {
    this.setState({
      modalOpen: true,
    });
  }

  handleClose() {
    this.setState({
      modalOpen: false,
    });
  }
  changeMainComponent(name) {
    this.setState({
      activeComponent: name,
      open: false,
    });
  }
  switchMainContainer() {
    const { activeComponent } = this.state;
    switch (activeComponent) {
      case 'fleetBoard':
        return <FleetBoard />;
      case 'fleetDataBase':
        return <FleetDataBase />;
      default:
        return <FleetBoard />;
    }
  }
  render() {
    const { open, modalOpen } = this.state;
    const { user, actions, truck, notifications } = this.props;
    const eventsListener = new FormEventsEmitter();
    const modalControls = [
      <FlatButton
        label="Anuluj"
        secondary={true}
        onClick={this.handleClose}
        style={{
          color: "#F44336",
        }}

      />,
      truck.status !== 'FETCHING' ?
        <FlatButton
          label="Wyślij"
          onClick={() => eventsListener.emit('submit')}
          style={{
            color: "#006cb7",
            marginBottom: 10,
            marginRight: 10,
          }}
        />
        :
        <FlatButton
        label="Wyślij"
        disabled={true}
        onClick={() => eventsListener.emit('submit')}
        style={{
          marginBottom: 10,
          marginRight: 10,
        }}
        />,
    ];
    return (
      <MuiThemeProvider>
        <div>
          <AppBar
            className="appbar-container"
            title={<div>Użytkownik: {user.email}</div>}
            onLeftIconButtonTouchTap={this.drawerToggle}
            titleStyle={{
              fontSize: "16px",
            }}
            iconElementRight={
              <FlatButton
                label="Wyloguj"
                onClick={() => actions.logOut()}
              />
            }
          />
          <Drawer
            open={open}
            docked={false}
            onRequestChange={open => this.setState({ open })}
          >
            <AppBar
              title="Menu"
              className="menubar-container"
              titleStyle={{
                fontSize: "22px",
              }}
              iconElementLeft={<IconButton onClick={this.drawerToggle}><NavigationClose /></IconButton>}
            />
            <MenuItem
              onClick={() => this.changeMainComponent('fleetBoard')}
              leftIcon={<DashboardIcon />}
              primaryText="Fleet board"
            />
            <MenuItem
              onClick={() => this.changeMainComponent('fleetDataBase')}
              leftIcon={<ContactIcon />}
              primaryText="Baza spedycji"
            />
          </Drawer>
          <div className="content-container">
            <div className="addButtonContainer">
              <FloatingActionButton
                onClick={this.handleOpen}
                backgroundColor="#8F2D56"
                style={{
                  marginTop: 15,

                }}

              >
                <ContentAdd />
              </FloatingActionButton>
              <div className="addText">Dodaj nowy pojazd</div>
            </div>

            {this.switchMainContainer()}
          </div>
          <Dialog
            modal={true}
            open={modalOpen}
            autoScrollBodyContent={true}
            actions={modalControls}
            onRequestClose={this.handleClose}
            bodyStyle={{
              minHeight: '500px',
            }}
          >
            <FleetForm
              eventsListener={eventsListener}
              closeModal={this.handleClose}
            />
          </Dialog>
          <Snackbar
            open={notifications.show}
            message={notifications.message}
          />
        </div>
      </MuiThemeProvider>
    );
  }
}

function mapDispatchToProps(dispatch) {
  return {
    actions: bindActionCreators(actions, dispatch),
  };
}

function mapStateToProps(state) {
  return {
    user: state.user,
    truck: state.truck,
    notifications: state.notifications,
  };
}
export default connect(mapStateToProps, mapDispatchToProps)(Dashboard);
