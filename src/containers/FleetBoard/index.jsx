import React, { Component } from 'react';
import { FleetList, FleetForm } from "../../components/index";

class FleetBoard extends Component {
  render() {
    return (
      <div>
        <div className="fleetHeader" />
        <div className="fleetBody">
          <FleetList />
        </div>
      </div>
    );
  }
}

export default FleetBoard;
