import React, { Component } from 'react';
import { TextField, SubmitButton, SelectField, AutoCompleteField } from "../../components/FormFields/index";
import Schema from 'form-schema-validation';
import { Form } from 'react-components-form';

const schema = new Schema({
  intial: {
    type: String,
    required: true,
  },
  username: {
    type: String,
    required: true,
  },
}, false, false);

const options = [
  { label: "Chłodnia", value: 33 },
  { label: "Plandeka", value: 20 },
  { label: "Silos", value: 10, disabled: true },
];

const members = [
  { name: "name1", surname: "surname1", age: "1" },
  { name: "name2", surname: "surname2", age: "2" },
  { name: "name3", surname: "surname3", age: "3" },
  { name: "name4", surname: "surname4", age: "4" },

];


// Teach Autosuggest how to calculate suggestions for any given input value.
const getSuggestions = (value) => {
  const inputValue = value.trim().toLowerCase();
  const inputLength = inputValue.length;

  return inputLength === 0 ? [] : languages.filter(lang =>
    lang.name.toLowerCase().slice(0, inputLength) === inputValue);
};

// When suggestion is clicked, Autosuggest needs to populate the input element
// based on the clicked suggestion. Teach Autosuggest how to calculate the
// input value for every given suggestion.
const getSuggestionValue = suggestion => suggestion.name;

// Use your imagination to render suggestions.
const renderSuggestion = suggestion => (
  <div>
    {suggestion.name}{suggestion.surname}
  </div>
);

class FleetDataBase extends Component {
  render() {
    return (
      <div>
        FleetDataBase

        <Form
          onSubmit={data => console.log('wysyłam', data)}
          onError={data => console.log('erros', data)}
          schema={schema}
        >
          <TextField
            name="intial"
            label="Pojazd"
            placeholder="Wpisz jakieś auto"
            fieldAttributes={{
              underlineFocusStyle: { borderColor: '#006cb7' },
              floatingLabelFocusStyle: { color: '#006cb7' },
            }}
          />
          <SelectField
            name="vehicle"
            label="Typ pojazdu"
            options={options}
          />
          <div style={{ marginTop: "70px" }}>
            <AutoCompleteField
              name="username"
              options={members}
              label="Pojazd"
              searchKey="name"
              getValue={option => option.name}
              renderItem={renderSuggestion}
            />
          </div>

          <SubmitButton value="Wyślij" />
        </Form>
      </div>
    );
  }
}

export default FleetDataBase;
