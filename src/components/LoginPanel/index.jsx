import React, { Component } from 'react';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import { TextField, RaisedButton, RefreshIndicator } from 'material-ui';
import { Link } from 'react-router-dom';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { loginByEmail } from "../../actions/userActions";


class FleetLogin extends Component {
  constructor() {
    super();
    this.state = {
      login: '',
      pass: '',
      loginValid: true,
      passValid: true,
    };
    this.userAuthenticate = this.userAuthenticate.bind(this);
    this.onInputChange = this.onInputChange.bind(this);
    this.loginValidation = this.loginValidation.bind(this);
    this.passValidation = this.passValidation.bind(this);
  }
  onInputChange(e) {
    const property = e.target.name;
    const value = e.target.value;
    this.setState({
      [property]: value,
    });
  }
  userAuthenticate(e) {
    e.preventDefault();
    const { loginByEmail } = this.props;
    const { login, pass } = this.state;
    if (login.length === 0) {
      return this.setState({
        loginValid: false,
      });
    } 
    this.setState({
      loginValid: true,
    });
    
    if (pass.length === 0) {
      return this.setState({
        passValid: false,
      });
    } 
    this.setState({
      passValid: true,
    });
    
    loginByEmail(login, pass);
  }
  loginValidation() {
    const { user } = this.props;
    const { loginValid } = this.state;
    if (!loginValid) {
      return 'Pole jest wymagane';
    }
    if (user.error && user.error.code === 'auth/invalid-email') {
      return 'Błędny adres email';
    }
    if (user.error && user.error.code === 'auth/user-not-found') {
      return 'Podany użytkownik nie istnieje';
    }
    return '';
  }
  passValidation() {
    const { user } = this.props;
    const { passValid } = this.state;
    if (!passValid) {
      return 'Pole jest wymagane';
    }
    if (user.error && user.error.code === 'auth/wrong-password') {
      return 'Błędne hasło';
    }
    return '';
  }
  render() {
    const { user } = this.props;
    return (
      <MuiThemeProvider>
        <div>
          <div className="register-button">
            <Link to="/register">
              <RaisedButton
                className="button-primary-style"
                buttonStyle={{
                  backgroundColor: "#006cb7",
                }}
                primary={true}
              >
                Załóź konto
              </RaisedButton>
            </Link>
          </div>
          <div className="login-container">
            <div className="login-box">
              <div className="svg-container">
                <svg fill="#006cb7" height="40" viewBox="0 0 24 24" width="40" xmlns="http://www.w3.org/2000/svg">
                  <path d="M12 12c2.21 0 4-1.79 4-4s-1.79-4-4-4-4 1.79-4 4 1.79 4 4 4zm0 2c-2.67 0-8 1.34-8 4v2h16v-2c0-2.66-5.33-4-8-4z" />
                  <path d="M0 0h24v24H0z" fill="none" />
                </svg>
              </div>
              <h3 className="login-header">Panel logowania</h3>
              <form onSubmit={this.userAuthenticate}>
                <div className="login-field">
                  <TextField
                      type="text"
                      name="login"
                      errorText={this.loginValidation()}
                      hintText="Login"
                      onChange={this.onInputChange}
                    />
                </div>
                <div className="login-field">
                  <TextField
                      type="password"
                      name="pass"
                      errorText={this.passValidation()}
                      hintText="Hasło"
                      onChange={this.onInputChange}
                    />
                </div>
                <div className="login-button">
                  {user.isFetching ?
                    <RefreshIndicator
                        size={30}
                        left={0}
                        top={0}
                        loadingColor="#006cb7"
                        status="loading"
                        style={{
                          display: "inline-block",
                          position: "relative",
                        }}
                      />
                      :
                    <RaisedButton
                        className="button-primary-style"
                        type="submit"
                        primary={true}
                        buttonStyle={{
                          backgroundColor: "#006cb7",
                        }}
                      >
                        Zaloguj się
                    </RaisedButton>
                    }
                </div>
              </form>
            </div>
          </div>
        </div>
      </MuiThemeProvider>
    );
  }
}

function mapStateToProps(state) {
  return {
    user: state.user,
  };
}

function mapDispatchToProps(dispatch) {
  return {
    loginByEmail: bindActionCreators(loginByEmail, dispatch),
  };
}
export default connect(mapStateToProps, mapDispatchToProps)(FleetLogin);
