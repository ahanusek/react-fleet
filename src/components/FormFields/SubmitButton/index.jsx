import React from 'react';
import { RaisedButton } from 'material-ui';
import { FieldConnect } from 'react-components-form';


const SubmitButton = ({
  wrapperClassName,
  className,
  submit,
  value,
  fieldAttributes = {},
}) => (
  <div className={wrapperClassName}>
    <RaisedButton
      onClick={submit}
      className={className}
      {...fieldAttributes}
      label={value}
    />
  </div>
);

export default FieldConnect(SubmitButton);
