import React from 'react';
import classnames from 'classname';

const ErrorField = ({ errors, errorClass, errorItemClass }) => (
  <div className={classnames(errorClass, 'defaultErrorClass')} >
    {errors.map((error, i) => <div key={i} className={errorItemClass}>{error}</div>)}
  </div>
);


ErrorField.defaultProps = {
  errors: [],
};

export default ErrorField;
