import React, { Component } from 'react';
import { Checkbox as CheckboxMaterial } from 'material-ui';


class Checkbox extends Component {
  constructor(props) {
    super(props);
    this.state = {
      checked: false,
    };
    this.toggleCheckbox = this.toggleCheckbox.bind(this);
  }
  toggleCheckbox() {
    this.setState({
      checked: !this.state.checked,
    });
  }
  render() {
    const { checked } = this.state;
    return (
      <CheckboxMaterial
            onClick={() => this.toggleCheckbox()}
            checked={checked}
            iconStyle={{
              fill: '#008744',
              color: '#008744',
            }}
          />
    );
  }
}

export default Checkbox;
