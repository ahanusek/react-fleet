import React from 'react';
import { FieldConnect } from 'react-components-form';
import { TextField as TextFieldMaterial } from 'material-ui';
import { ErrorField } from './../index.js';


const TextField = ({
  wrapperClassName,
  className,
  onChange,
  name,
  validationErrors,
  hasValidationError,
  value = '',
  label,
  placeholder,
  errorClass,
  errorClassItem,
  fieldAttributes = {},
}) => (
  <div className={wrapperClassName}>
    <TextFieldMaterial
      type="text"
      className={className}
      name={name}
      floatingLabelText={label}
      onChange={e => onChange(e.target.value)}
      value={value}
      hintText={placeholder}
      errorText={hasValidationError && <ErrorField errors={validationErrors} errorClass={errorClass} errorClassItem={errorClassItem} />}
      {...fieldAttributes}
    />
  </div>
);

export default FieldConnect(TextField);
