import React from 'react';
import { FieldConnect } from 'react-components-form';
import { SelectField as SelectFieldMaterial, MenuItem } from 'material-ui';
import { ErrorField } from './../index.js';


const SelectField = ({
  wrapperClassName,
  className,
  onChange,
  name,
  validationErrors,
  hasValidationError,
  value,
  options = [],
  label,
  placeholder,
  errorClass,
  errorClassItem,
  fieldAttributes = {},
  optionAttributes = {},
}) => (
  <div className={wrapperClassName}>
    <SelectFieldMaterial
      type="text"
      className={className}
      name={name}
      floatingLabelText={label}
      onChange={(e, key, payload) => onChange(payload)}
      value={value || null}
      hintText={placeholder}
      errorText={hasValidationError	 && <ErrorField errors={validationErrors} errorClass={errorClass} errorClassItem={errorClassItem} />}
      {...fieldAttributes}
    >
      {options.map(option => (
        <MenuItem
          key={option.label || option}
          value={option.label ? option.value : option}
          primaryText={option.label || option}
          disabled={option.disabled}
          {...optionAttributes}
        />
      ))}
    </SelectFieldMaterial>
  </div>
);

export default FieldConnect(SelectField);
