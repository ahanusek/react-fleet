import React, { Component } from 'react';
import _ from 'lodash';
import Checkbox from 'material-ui/Checkbox';
import {
  TableRow,
  TableRowColumn,
} from 'material-ui/Table';


class FleetItem extends Component {
  constructor(props) {
    super(props);
    this.state = {
      checked: false,
    };
    this.toggleCheckbox = this.toggleCheckbox.bind(this);
  }
  toggleCheckbox() {
    this.setState({
      checked: !this.state.checked,
    });
  }
  render() {
    const { item } = this.props;
    const { checked } = this.state;
    return (
      <TableRow
        key={item.id}
        selected={checked}
        children={<div>test</div>}
      >
        <TableRowColumn>
          <Checkbox
            onClick={() => this.toggleCheckbox()}
            checked={checked}
            iconStyle={{
              fill: '#008744',
              color: '#008744',
            }}
          />
        </TableRowColumn>
        <TableRowColumn>
          {_.get(item, 'basicInfo.carrier', '-')}
        </TableRowColumn>
        <TableRowColumn>
          {_.get(item, 'basicInfo.capacity', '-')}
        </TableRowColumn>
        <TableRowColumn>
          {_.get(item, 'basicInfo.route', '-')}
        </TableRowColumn>
        <TableRowColumn>
          {_.get(item, 'basicInfo.region', '-')}
        </TableRowColumn>
        <TableRowColumn>
          {_.get(item, 'basicInfo.freight', '-')} {_.get(item, 'basicInfo.currency', '')}
        </TableRowColumn>
        <TableRowColumn>
          {_.get(item, 'basicInfo.forwarder', '-')}
        </TableRowColumn>
        <TableRowColumn>
          {_.get(item, 'basicInfo.phoneNumber', '-')}
        </TableRowColumn>
      </TableRow>
    );
  }
}

export default FleetItem;
