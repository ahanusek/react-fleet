export const currencies = [
  { label: 'zł', value: 'PLN' },
  { label: '$', value: 'USD' },
  { label: '€', value: 'EUR' },
  { label: '£', value: 'GBP' },
];


export const regions = [
  { label: 'PL0', value: 'PL0' },
  { label: 'PL2', value: 'PL2' },
  { label: 'PL4', value: 'PL4' },
  { label: 'PL5', value: 'PL5' },
  { label: 'PL6', value: 'PL6' },
  { label: 'PL8', value: 'PL8' },
];

export const capacity = [
  { label: 'Naczepa 33 MPT', value: 'trailer' },
  { label: 'Solo 22 - 15 MPT', value: 'solo' },
  { label: 'Bus 10 - 8 MPT', value: 'bus' },
];

export const trailerType = [
  { label: 'Chłodnia', value: 'frigo' },
  { label: 'Plandeka', value: 'dry' },
  { label: 'Izoterma', value: 'izo' },
];
