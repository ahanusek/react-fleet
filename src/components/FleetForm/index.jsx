import React, { Component } from 'react';
import { Tabs, Tab } from 'material-ui/Tabs';
import { bindActionCreators } from 'redux';
import _ from 'lodash';
import { connect } from 'react-redux';
import LinearProgress from 'material-ui/LinearProgress';
import ErrorIcon from 'material-ui/svg-icons/alert/error-outline';
import { Form, ObjectField } from 'react-components-form';
import { TextField, SelectField, AutoCompleteField } from "../../components/FormFields/index";
import { currencies, regions, capacity, trailerType } from "./FleetForm.data";
import { addFormSchema } from "../../utils/addFormSchema/index";
import { addTruck } from "../../actions/truckActions";


class FleetForm extends Component {
  constructor(props) {
    super(props);
    this.state = {
      activeTab: 'basicData',
      basicDataHasErrors: false,
      additionalDataHasErrors: false,
    };
    this.handleChange = this.handleChange.bind(this);
    this.handleErrors = this.handleErrors.bind(this);
    this.onSubmit = this.onSubmit.bind(this);
  }

  handleChange(value) {
    this.setState({
      activeTab: value,
    });
  }
  handleErrors(errors) {
    console.log('errors', errors)
    if (!_.isEmpty(errors.companyInfo) || !_.isEmpty(errors.truckInfo)) {
      this.setState({
        additionalDataHasErrors: true,
      });
    } else {
      this.setState({
        additionalDataHasErrors: false,
      });
    }
    if (!_.isEmpty(errors.basicInfo)) {
      this.setState({
        basicDataHasErrors: true,
      });
    } else {
      this.setState({
        basicDataHasErrors: false,
      });
    }
  }
  onSubmit(data) {
    const { addTruck, closeModal } = this.props;
    this.setState({
      basicDataHasErrors: false,
    });
    addTruck(data).then(() => {
      closeModal();
    });
  }
  render() {
    const { activeTab, basicDataHasErrors, additionalDataHasErrors } = this.state;
    const { eventsListener, truck } = this.props;
    return (
      <div className="fleetFormContainer">
        <Form
            onSubmit={this.onSubmit}
            onError={this.handleErrors}
            schema={addFormSchema}
            eventsEmitter={eventsListener}
          >
          <Tabs
              value={activeTab}
              onChange={this.handleChange}
              tabItemContainerStyle={{
                backgroundColor: '#006cb7',
              }}
              inkBarStyle={{
                backgroundColor: '#FFBC42',
                height: 3,
              }}
            >
            <Tab
                label="Dane podstawowe"
                value="basicData"
                className="tabContent"
                icon={basicDataHasErrors && <ErrorIcon />}
              >
              <div className="container formContainer">
                <ObjectField name="basicInfo">
                  <div className="row">
                    <div className="col-sm-12 m-t-40">
                      <AutoCompleteField
                          name="carrier"
                          label="Przewoźnik"
                        />
                    </div>
                    <div className="col-sm-12">
                      <TextField
                          name="route"
                          label="Trasa/Kierunek"
                          fieldAttributes={{
                            underlineFocusStyle: { borderColor: '#006cb7' },
                            floatingLabelFocusStyle: { color: '#006cb7' },
                            fullWidth: true,
                          }}
                        />
                    </div>
                    <div className="col-sm-6">
                      <TextField
                          name="forwarder"
                          label="Spedytor"
                          fieldAttributes={{
                            underlineFocusStyle: { borderColor: '#006cb7' },
                            floatingLabelFocusStyle: { color: '#006cb7' },
                          }}
                        />
                    </div>
                    <div className="col-sm-6">
                      <TextField
                          name="phoneNumber"
                          label="Telefon kontaktowy"
                          fieldAttributes={{
                            underlineFocusStyle: { borderColor: '#006cb7' },
                            floatingLabelFocusStyle: { color: '#006cb7' },
                          }}
                        />
                    </div>
                    <div className="col-sm-6">
                      <SelectField
                          name="region"
                          label="Region"
                          options={regions}
                          fieldAttributes={{
                            underlineFocusStyle: { borderColor: '#006cb7' },
                            floatingLabelFocusStyle: { color: '#006cb7' },
                          }}
                        />
                    </div>
                    <div className="col-sm-3">
                      <TextField
                          name="freight"
                          label="Fracht"
                          fieldAttributes={{
                            underlineFocusStyle: { borderColor: '#006cb7' },
                            floatingLabelFocusStyle: { color: '#006cb7' },
                            fullWidth: true,
                          }}
                        />
                    </div>
                    <div className="col-sm-3">
                      <SelectField
                          name="currency"
                          label="Waluta"
                          options={currencies}
                          fieldAttributes={{
                            underlineFocusStyle: { borderColor: '#006cb7' },
                            floatingLabelFocusStyle: { color: '#006cb7' },
                            fullWidth: true,
                          }}
                        />
                    </div>
                    <div className="col-sm-6">
                      <SelectField
                          name="capacity"
                          label="Ładowność"
                          options={capacity}
                          fieldAttributes={{
                            underlineFocusStyle: { borderColor: '#006cb7' },
                            floatingLabelFocusStyle: { color: '#006cb7' },
                          }}
                        />
                    </div>
                    <div className="col-sm-6">
                      <SelectField
                          name="trailerType"
                          label="Typ zabudowy"
                          options={trailerType}
                          fieldAttributes={{
                            underlineFocusStyle: { borderColor: '#006cb7' },
                            floatingLabelFocusStyle: { color: '#006cb7' },
                          }}
                        />
                    </div>
                  </div>
                </ObjectField>
              </div>
            </Tab>
            <Tab
                label="Dodatkowe informacje"
                value="additionalData"
                className="tabContent"
                icon={additionalDataHasErrors && <ErrorIcon />}
              >
              <div className="container">
                <div className="row">
                  <div className="col-sm-12 centered">
                    <h5 className="formSubHeader">Dane pojazdu</h5>
                  </div>
                </div>
              </div>
              <div className="container formContainer">
                <ObjectField name="truckInfo">
                  <div className="row">
                    <div className="col-sm-12">
                      <div className="row">
                        <div className="col-sm-3">
                          <TextField
                              name="truckPlates"
                              label="Ciągnik"
                              placeholder="np. WBA 05056"
                              fieldAttributes={{
                                underlineFocusStyle: { borderColor: '#006cb7' },
                                floatingLabelFocusStyle: { color: '#006cb7' },
                                fullWidth: true,
                              }}
                            />
                        </div>
                        <div className="col-sm-3">
                          <TextField
                              name="trailerPlates"
                              label="Naczepa"
                              placeholder="np. WRA 05056"
                              fieldAttributes={{
                                underlineFocusStyle: { borderColor: '#006cb7' },
                                floatingLabelFocusStyle: { color: '#006cb7' },
                                fullWidth: true,
                              }}
                            />
                        </div>
                        <div className="col-sm-6">
                          <TextField
                              name="driver"
                              label="Kierowca"
                              fieldAttributes={{
                                underlineFocusStyle: { borderColor: '#006cb7' },
                                floatingLabelFocusStyle: { color: '#006cb7' },
                                fullWidth: true,
                              }}
                            />
                        </div>
                        <div className="col-sm-6">
                          <TextField
                              name="driverPhoneNumber"
                              label="Telefon kontaktowy"
                              fieldAttributes={{
                                underlineFocusStyle: { borderColor: '#006cb7' },
                                floatingLabelFocusStyle: { color: '#006cb7' },
                                fullWidth: true,
                              }}
                            />
                        </div>
                        <div className="col-sm-6">
                          <TextField
                              name="comment"
                              label="Uwagi"
                              fieldAttributes={{
                                underlineFocusStyle: { borderColor: '#006cb7' },
                                floatingLabelFocusStyle: { color: '#006cb7' },
                                fullWidth: true,
                                multiLine: true,
                              }}
                            />
                        </div>
                      </div>
                    </div>
                  </div>
                </ObjectField>
              </div>
              <div className="container">
                <div className="row">
                  <div className="col-sm-12 centered">
                    <h5 className="formSubHeader">Dane przewoźnika/spedytora</h5>
                  </div>
                </div>
              </div>
              <div className="container formContainer">
                <ObjectField name="companyInfo">
                  <div className="row">
                    <div className="col-sm-12">
                      <div className="row">
                        <div className="col-md-12">
                          <TextField
                              name="fullCompanyName"
                              label="Pełna nazwa przewoźnika"
                              fieldAttributes={{
                                underlineFocusStyle: { borderColor: '#006cb7' },
                                floatingLabelFocusStyle: { color: '#006cb7' },
                                fullWidth: true,
                                multiLine: true,
                              }}
                            />
                        </div>
                        <div className="col-md-6">
                          <TextField
                              name="vatNumber"
                              label="NIP"
                              fieldAttributes={{
                                underlineFocusStyle: { borderColor: '#006cb7' },
                                floatingLabelFocusStyle: { color: '#006cb7' },
                                fullWidth: true,
                              }}
                            />
                        </div>
                        <div className="col-md-6">
                          <TextField
                              name="locality"
                              label="Miejscowość"
                              fieldAttributes={{
                                underlineFocusStyle: { borderColor: '#006cb7' },
                                floatingLabelFocusStyle: { color: '#006cb7' },
                                fullWidth: true,
                              }}
                            />
                        </div>
                        <div className="col-md-6">
                          <TextField
                              name="street"
                              label="Ulica"
                              fieldAttributes={{
                                underlineFocusStyle: { borderColor: '#006cb7' },
                                floatingLabelFocusStyle: { color: '#006cb7' },
                                fullWidth: true,
                              }}
                            />
                        </div>
                        <div className="col-md-3">
                          <TextField
                              name="streetNumber"
                              label="Numer"
                              fieldAttributes={{
                                underlineFocusStyle: { borderColor: '#006cb7' },
                                floatingLabelFocusStyle: { color: '#006cb7' },
                                fullWidth: true,
                              }}
                            />
                        </div>
                        <div className="col-md-3">
                          <TextField
                              name="postalCode"
                              label="Kod pocztowy"
                              fieldAttributes={{
                                underlineFocusStyle: { borderColor: '#006cb7' },
                                floatingLabelFocusStyle: { color: '#006cb7' },
                                fullWidth: true,
                              }}
                            />
                        </div>
                        <div className="col-md-6">
                          <TextField
                              name="transID"
                              label="Trans Id"
                              fieldAttributes={{
                                underlineFocusStyle: { borderColor: '#006cb7' },
                                floatingLabelFocusStyle: { color: '#006cb7' },
                                fullWidth: true,
                              }}
                            />
                        </div>
                        <div className="col-md-6">
                          <TextField
                              name="email"
                              label="Adres e-mail"
                              fieldAttributes={{
                                underlineFocusStyle: { borderColor: '#006cb7' },
                                floatingLabelFocusStyle: { color: '#006cb7' },
                                fullWidth: true,
                              }}
                            />
                        </div>
                      </div>
                    </div>
                  </div>
                </ObjectField>
              </div>
            </Tab>
          </Tabs>
          {truck.status === 'FETCHING' ?
            <LinearProgress
                mode="indeterminate"
                color="#FFBC42"
                style={{
                  margin: '20px auto',
                  width: '80%',
                  position: 'absolute',
                  left: '10%',
                }}
              /> : null
            }
        </Form>
      </div>
    );
  }
}
function mapStateToProps(state) {
  return {
    truck: state.truck,
  };
}

function mapDispatchToProps(dispatch) {
  return {
    addTruck: bindActionCreators(addTruck, dispatch),
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(FleetForm);
