/* eslint-disable import/no-named-as-default */
import React from 'react';
import PropTypes from 'prop-types';
import { Switch, Route } from 'react-router-dom';
import LoginPanel from './LoginPanel/index.jsx';
import SignUpPanel from './SignUpPanel/index.jsx';
import Dashboard from './../containers/Dashboard/index.jsx';
import NotFoundPage from './NotFoundPage';

class App extends React.Component {
  render() {
    return (
      <Switch>
        <Route exact path="/" component={LoginPanel} />
        <Route path="/fleet" component={Dashboard} />
        <Route path="/register" component={SignUpPanel} />
        <Route component={NotFoundPage} />
      </Switch>
    );
  }
}

App.propTypes = {
  children: PropTypes.element,
};

export default App;
