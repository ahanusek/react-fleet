import React, { Component } from 'react';
import { connect } from 'react-redux';
import ReactTable from 'react-table';
import { bindActionCreators } from 'redux';
import { fetchTrucksList } from "../../actions/trucksListActions";
import { removeTruck } from "../../actions/truckActions";
import Loader from './../Loader/index.jsx';
import ItemCell from './../ItemCell/index.jsx';
import VehicleCell from './../TabelCells/VehicleCell/index.jsx';
import { Checkbox } from "../FormFields/index";
import ExtraRow from '../TabelCells/ExtraRow/index.jsx';

class FleetList extends Component {
  constructor(props) {
    super(props);
  }
  componentWillMount() {
    this.props.fetchTrucksList();
  }
  render() {
    const { trucksList } = this.props;
    return (
      <div>
        <ReactTable
          columns={[{
            Header: '',
            accessor: 'orderStatus', // String-based value accessors!
            maxWidth: 70,
            Cell: props => <Checkbox />,
          }, {
            Header: 'Spedycja',
            accessor: 'basicInfo.carrier',
            // Custom cell components!
          }, {
            Header: 'Kierunek/Trasa', // Custom header components!
            accessor: 'basicInfo.route',
          }, {
            Header: 'Pojazd',
            accessor: 'basicInfo.capacity',
            maxWidth: 180,
            Cell: props => <VehicleCell vehicle={props.original.basicInfo} />,
          },
            {
              Header: 'Region', // Custom header components!
              accessor: 'basicInfo.region',
              maxWidth: 80,
            },
            {
              Header: 'Fracht', // Custom header components!
              accessor: 'basicInfo.freight',
              maxWidth: 100,
              Cell: props => <div>{props.original.basicInfo.freight} {props.original.basicInfo.currency}</div>,
            },
            {
              Header: 'Spedytor', // Custom header components!
              accessor: 'basicInfo.forwarder',
              maxWidth: 170,
            },
            {
              Header: 'Telefon', // Custom header components!
              accessor: 'basicInfo.phoneNumber',
              maxWidth: 120,
            },
            {
              Header: 'Modyfikuj',
              accessor: 'modify',
              maxWidth: 100,
              Cell: props => <ItemCell removeTruck={this.props.removeTruck} truck={props.original} />,
            },
          ]}
          data={trucksList.trucks}
          minRows={1}
          showPagination={false}
          noDataText="Brak pozycji, dodaj przez formularz nowy pojazd"
          loading={trucksList.status === 'FETCHING'}
          loadingText={<Loader />}
          SubComponent={row => <ExtraRow data={row}/>}
        />
      </div>
    );
  }
}


function mapDispatchToProps(dispatch) {
  return {
    fetchTrucksList: bindActionCreators(fetchTrucksList, dispatch),
    removeTruck: bindActionCreators(removeTruck, dispatch),
  };
}

function mapStateToProps(state) {
  return {
    trucksList: state.trucksList,
  };
}
export default connect(mapStateToProps, mapDispatchToProps)(FleetList);
