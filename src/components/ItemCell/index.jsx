import React from 'react';
import IconButton from 'material-ui/IconButton';
import EditIcon from 'material-ui/svg-icons/editor/mode-edit';
import DeleteIcon from 'material-ui/svg-icons/action/delete';


const ItemCell = ({ truck, removeTruck }) => (
  <div>
    <IconButton
      tooltip="Edytuj"
      tooltipPosition="top-center"

      tooltipStyles={{
        top: '8px',
      }}
    >
      <EditIcon color="#757575" hoverColor="#006cb7" />
    </IconButton>
    <IconButton
      tooltip="Usuń"
      tooltipPosition="top-center"
      onClick={() => removeTruck(truck.id)}
      tooltipStyles={{
        top: '8px',
        left: '-2px',
      }}
    >
      <DeleteIcon color="#757575" hoverColor="#F44336" />
    </IconButton>
  </div>
);

export default ItemCell;
