
export const createLabelVehicle = (vehicleLabel) => {
  switch (vehicleLabel) {
    case 'trailer':
      return '33MPT';
    case 'solo':
      return 'SOLO';
    case 'bus':
      return 'BUS';
  }
};
