import React from 'react';
import { createLabelVehicle } from "./createLabelVehicle";

const VehicleCell = ({ vehicle }) => (
  <div>
    {createLabelVehicle(vehicle.capacity)}
  </div>
);

export default VehicleCell;
