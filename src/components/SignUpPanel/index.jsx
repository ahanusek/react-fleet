import React, { Component } from 'react';
import PropTypes from 'prop-types';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import { TextField, RaisedButton, RefreshIndicator } from 'material-ui';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { signUpByEmail } from "../../actions/registerActions";

class SignUpPanel extends Component {
  constructor() {
    super();
    this.state = {
      email: '',
      pass: '',
      passConfirm: '',
      emailValid: true,
      passValid: true,
      passConfirmValid: true,
      areSame: true,
      tooShort: false,
    };
    this.userRegistration = this.userRegistration.bind(this);
    this.onInputChange = this.onInputChange.bind(this);
    this.emailValidation = this.emailValidation.bind(this);
    this.passValidation = this.passValidation.bind(this);
    this.passConfirmValidation = this.passConfirmValidation.bind(this);
  }
  onInputChange(e) {
    const property = e.target.name;
    const value = e.target.value;
    this.setState({
      [property]: value,
    });
  }
  userRegistration(e) {
    e.preventDefault();
    const { signUpByEmail } = this.props;
    const { email, pass, passConfirm } = this.state;
    if (email.length === 0) {
      return this.setState({
        emailValid: false,
      });
    } 
    this.setState({
      emailValid: true,
    });
    
    if (pass.length === 0) {
      return this.setState({
        passValid: false,
      });
    } 
    this.setState({
      passValid: true,
    });
    
    if (passConfirm.length === 0) {
      return this.setState({
        passConfirmValid: false,
      });
    } 
    this.setState({
      passConfirmValid: true,
    });
    
    if (passConfirm !== pass) {
      return this.setState({
        areSame: false,
      });
    } 
    this.setState({
      areSame: true,
    });
    
    if (passConfirm.length < 5) {
      return this.setState({
        tooShort: true,
      });
    } 
    this.setState({
      tooShort: false,
    });
    
    signUpByEmail(email, pass);
  }
  emailValidation() {
    const { registration } = this.props;
    const { emailValid } = this.state;
    if (!emailValid) {
      return 'Pole jest wymagane';
    }
    if (registration.error && registration.error.code === 'auth/email-already-in-use') {
      return 'Ten adres email posiada już konto';
    }
    if (registration.error && registration.error.code === 'auth/invalid-email') {
      return 'Niepoprawny adres email';
    }
    return '';
  }
  passValidation() {
    const { passValid } = this.state;
    if (!passValid) {
      return 'Pole jest wymagane';
    }
  }
  passConfirmValidation() {
    const { passConfirmValid, areSame, tooShort } = this.state;
    if (!passConfirmValid) {
      return 'Pole jest wymagane';
    }
    if (!areSame) {
      return 'Hasła różnia się od siebie';
    }
    if (tooShort) {
      return 'Hasło musi mieć co najmniej 6 znaków';
    }
  }
  render() {
    const { registration } = this.props;
    return (
      <MuiThemeProvider>
        <div>
          <div className="register-button">
            <Link to="/">
              <RaisedButton
                className="button-primary-style"
                primary={true}
                buttonStyle={{
                  backgroundColor: "#006cb7",
                }}
              >
                Zaloguj się
              </RaisedButton>
            </Link>
          </div>
          <div>
            <div className="register-box">
              <div className="svg-container">
                <svg fill="#006cb7" height="40" viewBox="0 0 24 24" width="40" xmlns="http://www.w3.org/2000/svg">
                  <path d="M0 0h24v24H0z" fill="none" />
                  <path d="M15 12c2.21 0 4-1.79 4-4s-1.79-4-4-4-4 1.79-4 4 1.79 4 4 4zm-9-2V7H4v3H1v2h3v3h2v-3h3v-2H6zm9 4c-2.67 0-8 1.34-8 4v2h16v-2c0-2.66-5.33-4-8-4z" />
                </svg>
              </div>
              <h3 className="login-header">Załóż nowe konto</h3>
              <form onSubmit={this.userRegistration}>
                <div className="login-field">
                  <TextField
                    type="text"
                    name="email"
                    errorText={this.emailValidation()}
                    hintText="Login"
                    onChange={this.onInputChange}
                  />
                </div>
                <div className="login-field">
                  <TextField
                    type="password"
                    name="pass"
                    hintText="Hasło"
                    onChange={this.onInputChange}
                    errorText={this.passValidation()}
                  />
                </div>
                <div className="login-field">
                  <TextField
                    type="password"
                    name="passConfirm"
                    hintText="Potwierdź hasło"
                    onChange={this.onInputChange}
                    errorText={this.passConfirmValidation()}
                  />
                </div>
                <div className="login-button">
                  {registration.isFetching ?
                    <RefreshIndicator
                      size={30}
                      left={0}
                      top={0}
                      loadingColor="#006cb7"
                      status="loading"
                      style={{
                        display: "inline-block",
                        position: "relative",
                      }}
                    />
                    :
                    <RaisedButton
                      className="button-primary-style"
                      type="submit"
                      primary={true}
                      buttonStyle={{
                        backgroundColor: "#006cb7",
                      }}
                    >
                      Zarejestruj
                    </RaisedButton>
                  }
                </div>
              </form>
            </div>
          </div>
        </div>
      </MuiThemeProvider>
    );
  }
}

SignUpPanel.propTypes = {
  signUpByEmail: PropTypes.func,
};

function mapStateToProps(state) {
  return {
    registration: state.registration,
  };
}

function mapDispatchToProps(dispatch) {
  return {
    signUpByEmail: bindActionCreators(signUpByEmail, dispatch),
  };
}
export default connect(mapStateToProps, mapDispatchToProps)(SignUpPanel);
