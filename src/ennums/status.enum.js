export default {
  FETCHED: 'FETCHED',
  FETCHING: 'FETCHING',
  NEW: 'NEW',
};
