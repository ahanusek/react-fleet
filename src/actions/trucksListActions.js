import { firebaseRef } from './../firebase/firebaseConfigure';
import _ from 'lodash';
// import { notificationsCreator } from './notificationsCreator.js'

export const fetchTrucksList = () => (dispatch, getState) => {
  dispatch({ type: 'FETCH_TRUCKS_LIST_STARTED' });
  const { date } = getState();
  const trucksRef = firebaseRef.child(`trucks/${date}`);

  trucksRef.on('value', (snapshot) => {
    const trucks = snapshot.val() || {};
    const trucksArray = [];
    for (const key of Object.keys(trucks)) {
      trucksArray.push({
        id: key,
        ...trucks[key],
      });
    }
    const { trucksList } = getState();
    dispatch({ type: 'FETCH_TRUCKS_LIST_SUCCEEDED', trucks: trucksArray });
    if(_.isEmpty(trucksList.trucks)) {
      dispatch({ type: 'ADD_NOTIFICATION', message: 'POBRANO LISTĘ POJAZDÓW'});
    } else if(trucksArray.length > trucksList.trucks.length) {
      dispatch({ type: 'ADD_NOTIFICATION', message: 'DODANO NOWY POJAZD'});
    } else if(trucksArray.length < trucksList.trucks.length) {
      dispatch({ type: 'ADD_NOTIFICATION', message: 'USUNIĘTO POJAZD'});
    }
    setTimeout(() => {
      dispatch({ type: 'REMOVE_NOTIFICATION'});
    }, 4000);
  }, (errorObject) => {
    dispatch({ type: 'FETCH_TRUCKS_LIST_FAILED', error: errorObject });
  });
};



