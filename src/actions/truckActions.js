import { firebaseRef } from './../firebase/firebaseConfigure';
import moment from 'moment';

export const addTruck = truck => (dispatch, getState) => {
  dispatch({ type: 'ADD_TRUCK_STARTED' });
  const { date, user } = getState();
  const truckObject = {
    ...truck,
    timestamp: moment().format(),
    orderStatus: false,
    logs: [
      {
        type: 'created',
        ownerId: user.uid || null,
        ownerNick: user.email || null,
      }
    ],
  };
  return firebaseRef.child(`trucks/${date}`).push().set(truckObject)
    .then(() => {
      dispatch({ type: 'ADD_TRUCK_SUCCEEDED', truckObject });
    })
    .catch((e) => {
      dispatch({ type: 'ADD_TRUCK_FAILED', error: e });
    });
};

export const removeTruck = id => (dispatch, getState) => {
  dispatch({ type: 'REMOVE_TRUCK_STARTED' });
  const date = getState().date;
  const truckRef = firebaseRef.child(`trucks/${date}/${id}`);
  return truckRef.remove()
    .then(() => {
      dispatch({ type: 'REMOVE_TRUCK_SUCCEEDED' });
    })
    .catch((e) => {
      dispatch({ type: 'REMOVE_TRUCK_FAILED', error: e });
    });
};
