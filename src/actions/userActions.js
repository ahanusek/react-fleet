import firebase from './../firebase/firebaseConfigure';


export const loginByEmail = (email, pass) => (dispatch) => {
  dispatch({ type: 'LOGIN_STARTED' });
  const auth = firebase.auth();
  const login = auth.signInWithEmailAndPassword(email, pass);
  login
    .then((data) => {
      dispatch({ type: 'LOGIN_SUCCEEDED', user: data });
    })
    .catch((e) => {
      dispatch({ type: 'LOGIN_FAILED', error: e });
    });
};

export const logOut = () => (dispatch) => {
  dispatch({ type: 'LOGOUT_STARTED' });
  const auth = firebase.auth();
  const logout = auth.signOut();
  logout
    .then(() => dispatch({ type: 'LOGOUT_SUCCEEDED' }))
    .catch((e) => {
      dispatch({ type: 'LOGOUT_FAILED', error: e });
    });
};
