import firebase from './../firebase/firebaseConfigure';

export const signUpByEmail = (email, pass) => (dispatch) => {
  dispatch({ type: 'REGISTRATION_STARTED' });
  const auth = firebase.auth();
  const signup = auth.createUserWithEmailAndPassword(email, pass);

  signup
    .then((data) => {
      dispatch({ type: 'LOGIN_SUCCEEDED', user: data });
      dispatch({ type: 'REGISTRATION_SUCCEEDED' });
    })
    .catch((e) => {
      dispatch({ type: 'REGISTRATION_FAILED', error: e });
    });
};
