/* eslint-disable import/default */
import React from 'react';
import firebase from './firebase/firebaseConfigure';
import { render } from 'react-dom';
import { AppContainer } from 'react-hot-loader';
import configureStore, { history } from './store/configureStore';
import Root from './components/Root';
import './styles/styles.scss';

require('./favicon.ico');
// Tell webpack to load favicon.ico
const store = configureStore();

firebase.auth().onAuthStateChanged((user) => {
  if (user) {
    history.push('/fleet');
    const currentState = store.getState();
    if (currentState.user.uid === '') {
      store.dispatch({ type: 'LOGIN_SUCCEEDED', user });
    }
  } else {
    history.push('/');
  }
});

render(
  <AppContainer>
    <Root store={store} history={history} />
  </AppContainer>,
  document.getElementById('app'),
);

if (module.hot) {
  module.hot.accept('./components/Root', () => {
    const NewRoot = require('./components/Root').default;
    render(
      <AppContainer>
        <NewRoot store={store} history={history} />
      </AppContainer>,
      document.getElementById('app'),
    );
  });
}
