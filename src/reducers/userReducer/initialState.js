export default {
  uid: '',
  email: '',
  isFetching: false,
  fetched: false,
  error: {
    code: '',
    message: '',
  },
};
