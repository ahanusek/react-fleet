import initialState from './initialState';

export const userReducer = (state = initialState, action) => {
  switch (action.type) {
    case 'LOGIN_STARTED':
      return {
        ...state,
        isFetching: true,
      };
    case 'LOGIN_SUCCEEDED':
      return {
        ...initialState,
        isFetching: false,
        fetched: true,
        uid: action.user.uid,
        email: action.user.email,
      };
    case 'LOGIN_FAILED':
      return {
        ...initialState,
        isFetching: false,
        fetched: false,
        error: {
          code: action.error.code,
          message: action.error.message,
        },
      };
    case 'LOGOUT_STARTED':
      return {
        ...state,
        isFetching: true,
      };
    case 'LOGOUT_SUCCEEDED':
      return {
        ...initialState,
        isFetching: false,
        fetched: false,
      };
    case 'LOGOUT_FAILED':
      return {
        ...initialState,
        isFetching: false,
        fetched: false,
        error: {
          code: action.error.code,
          message: action.error.message,
        },
      };
    default:
      return state;
  }
};
