import initialState from './initialState';
import statusEnum from '../../ennums/status.enum';


export const truckReducer = (state = initialState, action) => {
  switch (action.type) {
    case 'ADD_TRUCK_STARTED':
      return {
        ...initialState,
        status: statusEnum.FETCHING,
      };
    case 'ADD_TRUCK_SUCCEEDED':
      return {
        ...state,
        truck: action.truck,
        status: statusEnum.FETCHED,
      };
    case 'ADD_TRUCK_FAILED':
      return {
        ...state,
        error: {
          code: action.error.code,
          message: action.error.message,
        },
        status: null,
      };
    case 'REMOVE_TRUCK_STARTED':
      return {
        ...initialState,
        status: 'REMOVING',
      };
    case 'REMOVE_TRUCK_SUCCEEDED':
      return {
        ...state,
        status: 'REMOVED',
      };
    case 'REMOVE_TRUCK_FAILED':
      return {
        ...state,
        error: {
          code: action.error.code,
          message: action.error.message,
        },
        status: null,
      };
    default:
      return state;
  }
};
