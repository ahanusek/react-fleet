import statusEnum from '../../ennums/status.enum';
import initialState from './initialState';

export const trucksListReducer = (state = initialState, action) => {
  switch (action.type) {
    case 'FETCH_TRUCKS_LIST_STARTED':
      return {
        ...state,
        status: statusEnum.FETCHING,
        error: {
          code: '',
          message: '',
        }
      };
    case 'FETCH_TRUCKS_LIST_SUCCEEDED':
      return {
        ...state,
        trucks: action.trucks,
        status: statusEnum.FETCHED,
      };
    case 'FETCH_TRUCKS_LIST_FAILED':
      return {
        ...state,
        error: {
          code: action.error.code,
          message: action.error.message,
        },
        status: null,
      };
    default:
      return state;
  }
};
