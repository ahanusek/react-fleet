import moment from 'moment';

export const dateReducer = (state = moment().format('DD-MM-YYYY'), action) => {
  switch (action.type) {
    case 'CHANGE_DATE':
      return action.date;
    default:
      return state;
  }
};
