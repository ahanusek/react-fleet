export default {
  isFetching: false,
  fetched: false,
  error: {
    code: '',
    message: '',
  },
};
