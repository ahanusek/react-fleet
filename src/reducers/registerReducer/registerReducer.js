import initialState from './initialState';

export const registerReducer = (state = initialState, action) => {
  switch (action.type) {
    case 'REGISTRATION_STARTED':
      return {
        ...state,
        isFetching: true,
      };
    case 'REGISTRATION_SUCCEEDED':
      return {
        ...state,
        isFetching: false,
        fetched: true,
      };
    case 'REGISTRATION_FAILED':
      return {
        ...state,
        isFetching: false,
        fetched: false,
        error: {
          code: action.error.code,
          message: action.error.message,
        },
      };
    default:
      return state;
  }
};
