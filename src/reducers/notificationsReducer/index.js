import initialState from './initialState';

export const notificationsReducer = (state = initialState, action) => {
  switch (action.type) {
    case 'ADD_NOTIFICATION':
      return {
        ...initialState,
        show: true,
        message: action.message,
      };
    case 'REMOVE_NOTIFICATION':
      return {
        ...initialState,
        show: false,
      };
    default:
      return state;
  }
};
