import { combineReducers } from 'redux';
import { routerReducer } from 'react-router-redux';
import { userReducer as user } from './userReducer/userReducer';
import { registerReducer as registration } from './registerReducer/registerReducer';
import { truckReducer as truck } from './truckReducer/truckReducer';
import { notificationsReducer as notifications } from "./notificationsReducer/index";
import { dateReducer as date } from "./dateReducer/dateReducer";
import { trucksListReducer as trucksList } from "./trucksListReducer/trucksListReducer";

const rootReducer = combineReducers({
  user,
  registration,
  truck,
  date,
  trucksList,
  notifications,
  routing: routerReducer,
});

export default rootReducer;
