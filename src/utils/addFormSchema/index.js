import Schema from 'form-schema-validation';

const ErrorMessages = {
  notDefinedKey(key) { return `Key '${key}' is not defined in schema`; },
  modelIsUndefined() { return 'Validated model is undefined'; },
  validateRequired() { return `Pole jest wymagane`; },
  validateString(key) { return `Field '${key}' is not a String`; },
  validateNumber(key) { return `Field '${key}' is not a Number`; },
  validateObject(key) { return `Field '${key}' is not a Object`; },
  validateArray(key) { return `Field '${key}' is not a Array`; },
  validateBoolean(key) { return `Field '${key}' is not a Boolean`; },
  validateDate(key) { return `Field '${key}' is not a Date`; },
};


export const truckInfo = new Schema({
  truckPlates: {
    type: String,
  },
  trailerPlates: {
    type: String,
  },
  driver: {
    type: String,
  },
  driverPhoneNumber: {
    type: String,
  },
  comment: {
    type: String,
  },
}, ErrorMessages, false);

export const companyInfo = new Schema({
  fullCompanyName: {
    type: String,
  },
  vatNumber: {
    type: String,
  },
  locality: {
    type: String,
  },
  street: {
    type: String,
  },
  streetNumber: {
    type: String,
  },
  postalCode: {
    type: String,
  },
  email: {
    type: String,
  },
  transID: {
    type: String,
  },
}, ErrorMessages, false);

export const basicInfo = new Schema({
  carrier: {
    type: String,
    required: true,
  },
  route: {
    type: String,
    required: true,
  },
  forwarder: {
    type: String,
    required: true,
  },
  freight: {
    type: String,
    required: true,
  },
  region: {
    type: String,
    defaultValue: 'PL0',
    required: true,
  },
  currency: {
    type: String,
    defaultValue: 'PLN',
    required: true,
  },
  trailerType: {
    type: String,
    required: true,
    defaultValue: 'frigo',
  },
  capacity: {
    type: String,
    required: true,
    defaultValue: 'trailer',
  },
  phoneNumber: {
    type: String,
    required: true,
  },
}, ErrorMessages, false);


export const addFormSchema = new Schema({
  basicInfo: {
    type: basicInfo,
  },
  truckInfo: {
    type: truckInfo,
  },
  companyInfo: {
    type: companyInfo,
  },
}, ErrorMessages, false);
